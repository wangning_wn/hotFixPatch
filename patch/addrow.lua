interface{"HFXTestClass"}
function output(self)
return ('[HFXTestClass] This is replaced output.')
end

interface{'HFXViewController'}
function tableView_numberOfRowsInSection(self, tableView, section)
    if (section == 1)
    then
        return 3
    else
        print("cttest in number of rows, section=" .. section )
        return self:ORIGtableView_numberOfRowsInSection(tableView, section)
    end
end
function cellTitleForIndexPath(self, indexPath)
    print("cttest section=" .. indexPath:section())
    if (indexPath:section() == 1 and indexPath:row() == 2)
    then
        return "新增行"
    else
        print("cttest in cell title, section=" .. indexPath:section())
        return self:ORIGcellTitleForIndexPath(indexPath)
    end
end
